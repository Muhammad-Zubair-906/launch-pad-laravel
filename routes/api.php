<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('user-login', 'Api\User\Auth\LoginController@login');
Route::post('user-register', 'Api\User\Auth\RegisterController@register');


Route::group(['middleware' => ['assign.guard:api','jwt.auth']],function () {
    Route::get('/token_demo','Api\User\UserController@demo');
});

