<?php

namespace App\Http\Controllers\Api\User\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\JsonReturn;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    
    function register(Request $request){
        DB::table("users")->insert([
            "name" => $request->name,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "created_at" => date("Y-m-d"),
            "updated_at" => date("Y-m-d")
            ]);
            
            $data = ['message' => 'Supervisor is Added'];
            return JsonReturn::success($data);
    }
}
