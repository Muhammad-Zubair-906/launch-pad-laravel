<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\JsonReturn;
use Illuminate\Http\Request;
use Config;
use Auth;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\DB;
use Validator;
use JWTFactory;
use JWTAuthException;

class LoginController extends Controller
{

public function login(Request $request){

        config(['auth.defaults.guard' => 'api']);

        Config::set('jwt.user', 'App\User');
        Config::set('auth.providers.users.model', \App\User::class);

        $credentials = $request->only('email', 'password');
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                $response = [
                    'response' => 'error',
                    'errorType'=> 'InvalidCredentials',
                    'message' => 'Incorrect email or password',
                ];
                return JsonReturn::error($response);
            }
        } catch (JWTAuthException $e) {
            $response = [
                'response' => 'error',
                'errorType'=> 'JWTTokenError',
                'message' => 'Token cannot be created',
            ];
            return JsonReturn::error($response);
        }

        $logged = User::where('email', '=', $request->email)->first();

        $response = [
            'token' => $token,
            'logged-user-info' => $logged,
            'message' => 'Logged in via user account',
        ];

        return JsonReturn::success($response);
    }


}
